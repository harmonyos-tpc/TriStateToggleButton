# TriStateToggleButton

TriStateToggleButton : A fully customizable and super-easy tri-state toggle button (switch button if you prefer) for OpenHarmony. Can act with three independent states,
or with two states like a standard checkbox, or with two states plus one undefined.

TriStateToggleButton includes
* Out-of-the-box working 3-state toggle.
* Fully customizable and styleable.
* Can become a classic 2-state toggle returning booleans.
* Can become a 2.5-state toggle: on/off and an unselectable mid button.
* Can be enabled / disabled.
* Can be programmatically controlled.
* Works both with clicks and swipes.

# Usage Instructions
Add namespace for app in layout root.
```
xmlns:app="http://schemas.huawei.com/hap/res-auto"

```

1. Basic implementation:
Layout:
```
<it.beppi.tristatetogglebutton_library.TriStateToggleButton
    ohos:id="$+id:tstb_1"
    ohos:height="70vp"
    ohos:width="140vp"/>
```

2. To have a two-states toggle button:
Layout:
```
<it.beppi.tristatetogglebutton_library.TriStateToggleButton
    ohos:id="$+id:tstb_2"
    ohos:height="70vp"
    ohos:width="140vp"
    app:tbIsMidSelectable="false"/>
```

3. To have a two-states toggle button, with an undefined starting value:
Layout:
```
<it.beppi.tristatetogglebutton_library.TriStateToggleButton
    ohos:id="$+id:tstb_3"
    ohos:height="70vp"
    ohos:width="140vp"
    app:tbDefaultStatus="mid"
    app:tbIsMidSelectable="false"/>
```

Listener: Compulsorily to be used with all attributes.
```
TriStateToggleButton tstb_1 = (TriStateToggleButton) findComponentById(ResourceTable.Id_tstb_1);
tstb_1.setOnToggleChanged(new TriStateToggleButton.OnToggleChanged() {
    @Override
    public void onToggle(TriStateToggleButton.ToggleStatus toggleStatus, boolean booleanToggleStatus, int toggleIntValue) {
        switch (toggleStatus) {
            case off:
                tstb_1_text.setText("Off");
                break;
            case mid:
                tstb_1_text.setText("Half way");
                break;
            case on:
                tstb_1_text.setText("On");
                break;
        }
    }
});
```

Attributes description

Attributes               | Description
----                     | ----
tbBorderWidth            |Width of the border of the widget.
tdOffBorderColor         |Color of the width that appears with the button in state off. Used also for animations.
tbOffColor               |Color of the background of the toggle when off.
tbMidColor               |Color of the background of the toggle with in mid position.
tbOnColor                |Color of the background of the toggle when on.
tbSpotColor              |Color of the handle of the toggle.
tbAnimate                |True for animation.
tbDefaultStatus          |Starting value for the toggle.
tbIsMidSelectable        |If false, the toggle becomes a standard two-states toggle,but can still assume the mid value if forced programmatically or set as default.
tbSwipeSensitivityPixels |Number of pixels a swipe must travel to fire a toggle event. Default is 200.If set to zero, swipes are disabled.

# Installation Instructions
1.For using TriStateToggleButton module in sample application, include the below library dependency to generate hap/tristatetogglebutton_library.har.

Modify entry build.gradle as below :
```
dependencies {
    implementation project(path: ':tristatetogglebutton_library')
}
```

2.For using TriStateToggleButton in separate application, add the "tristatetogglebutton_library.har" in libs folder of "entry" module.

Modify entry build.gradle as below :

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.har'])
}
```

3.For using TriStateToggleButton from a remote repository in separate application, add the below dependency in entry/build.gradle file.

Modify entry build.gradle as below :

```
dependencies {
    implementation 'io.openharmony.tpc.thirdlib:TriStateToggleButton:1.0.0'
}
```

# License

The MIT License (MIT)
```
Copyright (c) 2016 Beppi Menozzi

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
