/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.beppi.tristatetogglebuttonsample.slice;

import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import it.beppi.tristatetogglebuttonsample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;

import java.util.Random;

/**
 * Function description
 * class MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initializeViews();
    }

    private void initializeViews() {
        TriStateToggleButton triStateToggleButton1 = (TriStateToggleButton) findComponentById(ResourceTable.Id_tstb_1);
        Text triStateToggleButtonText1 = (Text) findComponentById(ResourceTable.Id_tstb_1_text);

        TriStateToggleButton triStateToggleButton2 = (TriStateToggleButton) findComponentById(ResourceTable.Id_tstb_2);
        Text triStateToggleButtonText2 = (Text) findComponentById(ResourceTable.Id_tstb_2_text);

        TriStateToggleButton triStateToggleButton3 = (TriStateToggleButton) findComponentById(ResourceTable.Id_tstb_3);
        Text triStateToggleButtonText3 = (Text) findComponentById(ResourceTable.Id_tstb_3_text);

        TriStateToggleButton triStateToggleButton4 = (TriStateToggleButton) findComponentById(ResourceTable.Id_tstb_4);

        // Example 1: a default tristate toggle without any customization
        triStateToggleButtonText1.setText(ResourceTable.String_off);
        triStateToggleButton1.setOnToggleChanged((toggleStatus, booleanToggleStatus, toggleIntValue) -> {
            switch (toggleStatus) {
                case off:
                    triStateToggleButtonText1.setText(ResourceTable.String_off);
                    break;
                case mid:
                    triStateToggleButtonText1.setText(ResourceTable.String_half_way);
                    break;
                case on:
                    triStateToggleButtonText1.setText(ResourceTable.String_on);
                    break;
            }
        });

        // Example 2: a default tristate toggle that starts in the middle status and never gets back again to it
        triStateToggleButtonText2.setText(ResourceTable.String_almost);
        triStateToggleButton2.setOnToggleChanged((toggleStatus, booleanToggleStatus, toggleIntValue) -> {
            switch (toggleStatus) {
                case off:
                    triStateToggleButtonText2.setText(ResourceTable.String_off);
                    break;
                case mid:
                    triStateToggleButtonText2.setText(ResourceTable.String_almost);
                    break;
                case on:
                    triStateToggleButtonText2.setText(ResourceTable.String_on);
                    break;
            }
        });

        // Example 3: a customized tristate toggle that is undefined in the middle and enables / disables another toggle
        triStateToggleButtonText3.setText(ResourceTable.String_undefined);
        triStateToggleButton3.setOnToggleChanged((toggleStatus, booleanToggleStatus, toggleIntValue) -> {
            switch (toggleStatus) {
                case off:
                    triStateToggleButtonText3.setText(ResourceTable.String_disabled);
                    break;
                case mid:
                    triStateToggleButtonText3.setText(ResourceTable.String_undefined);
                    break;
                case on:
                    triStateToggleButtonText3.setText(ResourceTable.String_enabled);
                    break;
            }
        });

        // Example 4: an out of the box classic 2-state toggle, using booleans, controls toggle 3
        triStateToggleButton4.setToggleStatus(true);
        triStateToggleButton4.setOnToggleChanged((toggleStatus, booleanToggleStatus, toggleIntValue)
                -> triStateToggleButton3.setEnabled(booleanToggleStatus));

        // Example 5: random restyle of toggle 3
        findComponentById(ResourceTable.Id_button_restyle)
                .setClickedListener(component -> {
                    restyle(triStateToggleButton3);
                });
    }

    private void restyle(TriStateToggleButton toggleButton) {
        Random random = new Random();
        for (int index = 0; index < 7; index++) {
            int randomColor = Color.argb(255,
                    random.nextInt(256), random.nextInt(256), random.nextInt(256));
            switch (index) {
                case 0:
                    toggleButton.setMidColor(randomColor);
                    break;
                case 1:
                    toggleButton.setBorderColor(randomColor);
                    break;
                case 2:
                    toggleButton.setOffBorderColor(randomColor);
                    break;
                case 3:
                    toggleButton.setOffColor(randomColor);
                    break;
                case 4:
                    toggleButton.setOnColor(randomColor);
                    break;
                case 5:
                    toggleButton.setSpotColor(randomColor);
                    break;
                case 6:
                    toggleButton.setSpotSize(random.nextInt(20) + 30);
                    break;
            }
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
