/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.beppi.tristatetogglebuttonsample;

import it.beppi.tristatetogglebutton_library.TriStateToggleButton;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Function description
 * class TriStateToggleButtonTest
 */
public class TriStateToggleButtonTest {
    private TriStateToggleButton triStateToggleButton;

    @Before
    public void getTriStateToggleButtonObject() {
        triStateToggleButton = new TriStateToggleButton(AbilityDelegatorRegistry.getAbilityDelegator().getAppContext());
    }

    /**
     * setAnimateTest
     */
    @Test
    public void setAnimateTest() {
        triStateToggleButton.setAnimate(true);
        assertTrue(triStateToggleButton.isAnimate());
    }

    /**
     * setOnColorTest
     */
    @Test
    public void setOnColorTest() {
        int color = Color.getIntColor("#ffca28");
        triStateToggleButton.setOnColor(color);
        assertEquals(color, triStateToggleButton.getOnColor());
    }

    /**
     * setBorderColor
     */
    @Test
    public void setBorderColorTest() {
        int color = Color.getIntColor("#ffca28");
        triStateToggleButton.setBorderColor(color);
        assertEquals(color, triStateToggleButton.getBorderColor());
    }

    /**
     * setMidColor
     */
    @Test
    public void setMidColorTest() {
        int color = Color.getIntColor("#ffca28");
        triStateToggleButton.setMidColor(color);
        assertEquals(color, triStateToggleButton.getMidColor());
    }

    /**
     * setOffBorderColor
     */
    @Test
    public void setOffBorderColorTest() {
        int color = Color.getIntColor("#ffca28");
        triStateToggleButton.setOffBorderColor(color);
        assertEquals(color, triStateToggleButton.getOffBorderColor());
    }

    /**
     * setOffColor
     */
    @Test
    public void setOffColorTest() {
        int color = Color.getIntColor("#ffca28");
        triStateToggleButton.setOffColor(color);
        assertEquals(color, triStateToggleButton.getOffColor());
    }

    /**
     * setSpotColor
     */
    @Test
    public void setSpotColorTest() {
        int color = Color.getIntColor("#ffca28");
        triStateToggleButton.setSpotColor(color);
        assertEquals(color, triStateToggleButton.getSpotColor());
    }

    /**
     * setSpotSize
     */
    @Test
    public void setSpotSizeTest() {
        int color = Color.getIntColor("#ffca28");
        triStateToggleButton.setSpotSize(color);
        assertEquals(color, triStateToggleButton.getSpotSize());
    }

    /**
     * setBorderWidth
     */
    @Test
    public void setBorderWidthTest() {
        triStateToggleButton.setBorderWidth(12);
        assertEquals(12, triStateToggleButton.getBorderWidth());
    }

    /**
     * isMidSelectable
     */
    @Test
    public void isMidSelectableTest() {
        triStateToggleButton.setMidSelectable(true);
        assertTrue(triStateToggleButton.isMidSelectable());
    }
}
