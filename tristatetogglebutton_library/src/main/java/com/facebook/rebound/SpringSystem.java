package com.facebook.rebound;

import ohos.app.Context;

/**
 * This is a wrapper for BaseSpringSystem that provides the convenience of automatically providing
 * the OpenHarmonySpringLooper dependency in {@link SpringSystem#create}.
 */
public class SpringSystem extends BaseSpringSystem {
    /**
     * Create a new SpringSystem providing the appropriate constructor parameters to work properly
     * in an OpenHarmony environment.
     *
     * @param context Context
     * @return the SpringSystem
     */
    public static SpringSystem create(Context context) {
        return new SpringSystem(OpenHarmonySpringLooperFactory.createSpringLooper(context));
    }

    private SpringSystem(SpringLooper springLooper) {
        super(springLooper);
    }
}
