package com.facebook.rebound.ui;

import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.solidxml.TypedAttribute;

/**
 * Utilities for generating view hierarchies without using resources.
 */
public abstract class Util {
    public static final int dpToPx(float dp, ResourceManager res) {
        return (int) TypedAttribute.computeTranslateRatio(res.getDeviceCapability());
    }

    public static final StackLayout.LayoutConfig createLayoutConfig(int width, int height) {
        return new StackLayout.LayoutConfig(width, height);
    }

    public static final StackLayout.LayoutConfig createMatchParams() {
        return createLayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT);
    }

    public static final StackLayout.LayoutConfig createWrapParams() {
        return createLayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
    }

    public static final StackLayout.LayoutConfig createWrapMatchParams() {
        return createLayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_CONTENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT);
    }

    public static final StackLayout.LayoutConfig createMatchWrapParams() {
        return createLayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_CONTENT);
    }
}
